from plyfile import PlyData, PlyElement
pathpref = '/export/home/kandinsky/chimeur/ABCData/abc_ply/'
filename  = 'wtest.txt'
file  =  open(filename,'r')
names = file.read().splitlines()
file.close(); 
for i in range(0,len(names)):
	name = names[i]
	flb = open(pathpref+name+'.lb','r')
	lbs = flb.read().splitlines()
	lbs = lbs[1:len(lbs)] 
	flb.close()
	f = open(pathpref+name+'.ply','r')
	data = PlyData.read(pathpref+name+'.ply')
	f.close()
	f = open(name+'.xyz','w')
	flb = open(name+'.normals','w')
	for j in range(0,data.elements[0].count):
		f.write(str(data['vertex'][j][0])+' '+str(data['vertex'][j][1])+' '+str(data['vertex'][j][2])+'\n')
		normal = ['0','0','0']
		normal[int(lbs[j])] = '1'
		flb.write(normal[0]+' '+normal[1]+' '+normal[2]+'\n')


